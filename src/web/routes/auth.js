import Config from '../config';
import { TEMPLATE_TYPES } from '../templates/index';
import LoginComponent from '../../modules/auth/UI/login';
import SignUpComponent from '../../modules/auth/UI/signup';

const loginRoute = {
  Component: LoginComponent,
  path: '/login',
  title: Config.APP_TITLE,
  templateType: TEMPLATE_TYPES.GUEST,
};

const signupRoute = {
  Component: SignUpComponent,
  path: '/signup',
  title: Config.APP_TITLE,
  templateType: TEMPLATE_TYPES.GUEST,
}

export default [
  loginRoute,
  signupRoute,
]