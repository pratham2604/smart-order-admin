import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { TEMPLATE } from '../templates/index';
import AuthContainer from '../../containers/auth';
import Config from '../config';

import HomeRoute from './home';
import AuthRoute from './auth';
import MenuRoute from './menu';
import TableRoute from './table';

const Routes = [
  HomeRoute,
  ...AuthRoute,
  MenuRoute,
  TableRoute,
];

export default () => (
  <Switch>
    {Routes.map((route, index) => {
      const { Component, Container, path, title, exact, templateType, hideFooter = false } = route;
      const Template = TEMPLATE[templateType || Config.DEFAULT_TEMPLATE];
      return (
        <Route
          key={index}
          exact={exact}
          path={path}
          render={props => {
            return (
              <AuthContainer
                Template={Template}
                pageTitle={title}
                hideFooter={hideFooter}
                Container={Container}
                Component={Component}
                routeProps={props}
              />
            )
          }}
        />
      )
    })}
  </Switch>
);