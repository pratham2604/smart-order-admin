import Config from '../config';
import { TEMPLATE_TYPES } from '../templates/index';
import TableContainer from '../../containers/table';
import TableComponent from '../../modules/tables/index';

export default {
  Component: TableComponent,
  Container: TableContainer,
  path: '/table',
  exact: true,
  title: Config.APP_TITLE,
  templateType: TEMPLATE_TYPES.AUTH_SIDEBAR,
};