import React, { Fragment, Component } from 'react';
import { Image, Menu, Container } from 'semantic-ui-react';
import { Helmet } from 'react-helmet';
import LOGO from '../../assets/logo.png';
const CLIENT_LINK = 'https://smartordr.com/';

export default class extends Component {
  render() {
    const { children, pageTitle } = this.props;
    return (
      <Fragment>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        <Menu fixed="top" secondary={true} size='large' className="guest-menu-nvabar">
          <Container>
            <Menu.Item as='h3'>
              <Image src={LOGO} size="mini" />
              <a className="app-title" href={CLIENT_LINK}>Smart Ordr</a>
            </Menu.Item>
          </Container>
        </Menu>
        {children}
      </Fragment>
    );
  }
}