import React, { Component } from 'react';
import _ from 'lodash';
import { Segment, Grid, Header, Input } from 'semantic-ui-react';
import QRModal from './UI/qrModal';
import OrderModal from './UI/orderModal';
import AddTable from './UI/addTable';

const titleMap = {
  hotel: 'Room',
  shop: 'Location',
  services: 'Location',
  others: 'Location'
}

const LETTER_REGEX = /[^a-zA-Z]/g;
const NUMBER_REGEX = /[^0-9]/g;

const sortAlphaNum = (aTable, bTable) => {
  var a = aTable.number;
  var b = bTable.number;
  var aA = a.replace(LETTER_REGEX, "");
  var bA = b.replace(LETTER_REGEX, "");
  if (aA === bA) {
    var aN = parseInt(a.replace(NUMBER_REGEX, ""), 10);
    var bN = parseInt(b.replace(NUMBER_REGEX, ""), 10);
    return aN === bN ? 0 : aN > bN ? 1 : -1;
  }
  return aA > bA ? 1 : -1;
}

export default class extends Component {
  state = {
    name: '',
  }

  onChange = (e, {name, value}) => {
    this.setState({
      name: value
    });
  }

  render() {
    const { restaurant = {}, tables, orders, addTable } = this.props;
    const type = titleMap[restaurant.type] || 'Table';
    const { name } = this.state;

    return (
      <Segment className="tables-container">
        <Grid stackable>
          <Grid.Row>
            <Grid.Column width={12}>
              <Header as="h1">
                {`${type}s`}
                <AddTable addTable={addTable} type={type} restaurantId={restaurant.id}/>
              </Header>
            </Grid.Column>
            <Grid.Column width={4}>
              <Input onChange={this.onChange} value={name} placeholder="Search"/>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Grid stackable style={{padding: '1em'}}>
          {tables.sort(sortAlphaNum)
            .filter(table => {
              const { number = '' } = table;
              return number.toLowerCase().includes(name.toLowerCase());
            })
            .map((table, index) => {
            return (
              <Grid.Column width={4} key={index}>
                <Segment inverted color="teal">
                  <Header as="h1">
                    {table.number}
                  </Header>
                  <Header as="h4">ID: {table.id}</Header>
                  <div style={{display: 'flex'}}>
                    <QRModal restaurant={restaurant} table={table} />
                    <OrderModal restaurant={restaurant} table={table} orders={orders}/>
                  </div>
                </Segment>
              </Grid.Column>
            )
          })}
        </Grid>
      </Segment>
    )
  }
}