import React, { Component } from 'react';
import { Grid, Header, Segment } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import FC from '../../../components/formsy/index';

class LoginForm extends Component {
  state = {
    canSubmit: false,
  }

  validations = {
    email: [{
      validator: 'isRequired',
    }, {
      validator: 'isEmail',
    }], 
    password: [{
      validator: 'isRequired',
    }]
  }

  enableButton = () => {
    this.setState({
      canSubmit: true,
    });
  }

  disableButton = () => {
    this.setState({
      canSubmit: false,
    });
  }

  onSubmit = (model) => {
    const { onLogin } = this.props;
    onLogin(model);
  }

  render() {
    const { canSubmit } = this.state;
    const { loading } = this.props;

    return (
      <Grid textAlign='center' className="auth-form" verticalAlign='middle'>
        <Grid.Column mobile={16} tablet={12} computer={8} style={{paddingRight: 0}}>
          <Segment raised className="form-container">
            <Header as='h2' className="form-header" textAlign='center'>
              Log in
            </Header>
            <Header as='h5' className="description">
              Access your business management system by entering your login details
            </Header>
            <FC.Form onValid={this.enableButton} onInvalid={this.disableButton} onSubmit={this.onSubmit} validations={this.validations}>
              <FC.Input fluid icon='mail' iconPosition='left' placeholder='E-mail address' name="email" formValidations={this.validations.email} required/>
              <FC.Input fluid icon='lock' iconPosition='left' placeholder='Password' name="password" type="password" formValidations={this.validations.password} required/>
              <FC.SubmitButton loading={loading} disabled={!canSubmit} className="submit-button" size='large' type="submit">LOGIN</FC.SubmitButton>
            </FC.Form>
            <div className="form-footer">
              New to us? <Link to="/signup">Sign Up</Link>
            </div>
          </Segment>
        </Grid.Column>
      </Grid>
    )
  }
}

export default LoginForm;