import React, { Component, Fragment } from 'react';
import { v4 as uuidv4 } from 'uuid';
import _ from 'lodash';
import { Button, Header, Icon, Modal, Grid } from 'semantic-ui-react';
import FC from '../../../../components/formsy/index';
import Customization from '../customization/customization';
import { CURRENCY_ICON_MAP } from '../../../../lib/currency';

const isArrayEqual = function(x, y) {
  return _(x).differenceWith(y, _.isEqual).isEmpty();
};

const GST_CATEGORIES = [{
  text: 'Food',
  value: 'FOOD',
}, {
  text: 'Liquor',
  value: 'LIQUOR',
}, {
  text: 'Soft Drink',
  value: 'SOFT_DRINK',
}, {
  text: 'Water',
  value: 'WATER',
}];

export default class ModalExampleControlled extends Component {
  state = {
    modalOpen: false,
    canEdit: false,
    submitted: false,
    customizations: (this.props.data || {}).customizations || [],
  }

  componentDidUpdate(prevProps) {
    const { menuAdded, data = {} } = this.props;
    const { customizations = [] } = data;
    const oldCustomizations = (prevProps.data || {}).customizations || [];
    const { submitted } = this.state;
    if (submitted && (menuAdded && !prevProps.menuAdded)) {
      this.setState({
        modalOpen: false,
        submitted: false,
      });
    }

    if (!isArrayEqual(customizations, oldCustomizations)) {
      this.setState({
        customizations,
      });
    }
  }

  handleOpen = (dimmer) => {
    this.setState({
      dimmer: 'blurring',
      modalOpen: true,
    });
  }

  handleClose = () => {
    this.setState({
      modalOpen: false,
    });
  }

  enableButton = () => {
    this.setState({
      canEdit: true,
    })
  }

  disableButton = () => {
    this.setState({
      canEdit: false,
    });
  }

  delete = () => {
    const { data, deleteItem } = this.props;
    const { id } = data;
    deleteItem(id);
    this.setState({
      submitted: true,
    });
  }

  updateCustomizations = (customizations) => {
    this.setState({
      customizations,
    });
  }

  onSubmit = (model) => {
    const { customizations } = this.state;
    const { id } = this.props.data || {};
    const data = Object.assign({}, model);
    const thumbnail = (data.menuImage || {}).link || '';
    data.thumbnail = thumbnail;
    data.id = id || uuidv4();
    data.price = parseFloat(data.price);
    data.customizations = customizations;
    if (!data.description) {
      delete data.description;
    }
    if (!data.ingredients) {
      delete data.ingredients;
    }
    if (!data.shortDescription) {
      delete data.shortDescription;
    }
    delete data.menuImage;
    this.props.updateMenu(data);
    this.setState({
      submitted: true,
    });
  }

  render() {
    const { edit, data, categories = [], restaurant = {} } = this.props;
    const { currency = 'INR' } = restaurant;
    const { modalOpen, dimmer, canEdit, customizations } = this.state;
    const trigger = <Tigger edit={edit} handleOpen={this.handleOpen} />;
    const title = edit ? 'Edit Menu item' : 'Add Menu item';
    const { ingredient, name, price, thumbnail, isAvailable, description, type, category, allowChefNote = false } = data || {};
    const image = {
      link: thumbnail
    };
    const gstCategory = type || GST_CATEGORIES[0].value;
    const foodCategories = categories.map(category => {
      return Object.assign({}, {
        value: category.id,
        text: category.name,
      });
    });
    const currency_icon = CURRENCY_ICON_MAP[currency];
    const hideFields = ['shop', 'services', 'others'].some(type => restaurant.type === type);
    const { country = {} } = restaurant;
    const{ id = '' } = country;
    const isNationIndia = id === 'IN';

    return (
      <Modal dimmer={dimmer} trigger={trigger} open={modalOpen} onClose={this.handleClose} centered={false} size="small" closeIcon>
        <Modal.Content>
          <Header as="h1">
            {title}
          </Header>
          <FC.Form onValid={this.enableButton} onInvalid={this.disableButton} onSubmit={this.onSubmit} validations={this.validations}>
            <Grid stackable columns={2} className="menu-container">
              <Grid.Row>
                <Grid.Column width={5}>
                  <FC.ImageUpload name="menuImage" mobile={16} tablet={16} computer={16} label="Menu Image" width="100%" value={image}/>
                </Grid.Column>
                <Grid.Column width={11}>
                  <FC.Input fluid placeholder='Name' name="name" required value={name}/>
                  <FC.Input fluid icon={currency_icon} iconPosition='left' placeholder='Price' name="price" value={price} type="number" required/>
                  <FC.Input fluid placeholder={hideFields ? 'Short description' : 'Ingredients'} name={hideFields ? 'shortDescription' : 'ingredients'} value={ingredient} required/>
                  <FC.Checkbox label="Available" name="isAvailable" toggle value={isAvailable} required/>
                  {!hideFields && <FC.Checkbox label="Allow Note for Chef" name="allowChefNote" toggle value={allowChefNote}/>}
                </Grid.Column>
              </Grid.Row>
              {!hideFields &&
                <Grid.Row>
                  <Grid.Column width={16}>
                    <Customization customizations={customizations} updateCustomizations={this.updateCustomizations} />
                  </Grid.Column>
                </Grid.Row>
              }
              <Grid.Row>
                <Grid.Column width={16}>
                  <FC.TextArea fluid placeholder='Description (optional)' name="description" value={description}/>
                </Grid.Column>
              </Grid.Row>
              {(!hideFields && isNationIndia) &&
                <Grid.Row>
                  <Grid.Column width={16}>
                    <FC.Select fluid placeholder='GST Category' label="Select GST Category" name="type" required value={gstCategory} options={GST_CATEGORIES} />
                  </Grid.Column>
                </Grid.Row>
              }
              <Grid.Row>
                <Grid.Column width={16}>
                  <FC.Select fluid placeholder='Category' label="Select Category" name="category" required value={category} options={foodCategories} />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <FC.SubmitButton disabled={!canEdit}>Save</FC.SubmitButton>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </FC.Form>
          {edit && <Button negative onClick={this.delete} style={{marginTop: '1em'}}>Delete</Button>}
        </Modal.Content>
      </Modal>
    )
  }
}

const Tigger = (props) => {
  const { edit, handleOpen } = props;
  return (
    <Fragment>
      {edit ?
        <Icon name="pencil" onClick={handleOpen} style={{cursor: 'pointer'}}/> :
        <Button color="blue" className="add-menu" onClick={handleOpen}>Add New</Button>
      }
    </Fragment>
  )
}
