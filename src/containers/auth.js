import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchAuth, signUp, signOut, login } from '../actions/auth';
import Loader from '../components/UI/loader/loader';

const GUEST_ROUTES = ['/login', '/signup'];

class Auth extends Component {
  state = {
    loadingAuth: true,
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchAuth());
  }

  componentDidUpdate(prevProps) {
    const { loadingAuth } = this.state;
    const { fetchingAuth, history, isAuthenticated, location } = this.props;
    if ((!fetchingAuth && prevProps.fetchingAuth) && loadingAuth) {
      const isGuestRoute = GUEST_ROUTES.find(route => route === location.pathname);
      if (isAuthenticated && isGuestRoute) {
        history.push('/');
      } else if (!isAuthenticated && !isGuestRoute) {
        history.push('/login');
      }
      this.setState({
        loadingAuth: false,
      });
    }
  }

  onSignUp = (data) => {
    const { dispatch } = this.props;
    dispatch(signUp(data));
    this.setState({
      loadingAuth: true,
    });
  }

  onLogin = (data) => {
    const { dispatch } = this.props;
    dispatch(login(data));
    this.setState({
      loadingAuth: true,
    });
  }

  signOut = () => {
    const { dispatch } = this.props;
    dispatch(signOut());
    this.setState({
      loadingAuth: true,
    });
  }

  render() {
    const { Template, pageTitle, hideFooter, Container, Component, routeProps, signingIn, profileData } = this.props;
    const { loadingAuth } = this.state;
    if (loadingAuth && !signingIn) {
      return (
        <Loader />
      );
    }

    const layout = Container ?
      <Container props={routeProps} Layout={Component} /> :
      <Component props={routeProps} onSignUp={this.onSignUp} onLogin={this.onLogin} loading={signingIn}/>

    return (
      <Template pageTitle={pageTitle} hideFooter={hideFooter} signOut={this.signOut} profileData={profileData}>
        {layout}
      </Template>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth.user,
  fetchingAuth: state.auth.fetching,
  isAuthenticated: state.auth.isAuthenticated,
  signingIn: state.auth.signingIn,
  profileData: state.restaurant.restaurant,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Auth));