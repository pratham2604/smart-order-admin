import { Firestore } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { toast } from 'react-toastify';
import { v4 as uuidv4 } from 'uuid';
import { SEND_SMS } from '../lib/smsHelper';

export const fetchRestaurant = (user) => {
  return (dispatch) => {
    const { uid } = user;
    dispatch({type: TYPES.FETCH_RESTAURANT_REQUEST});
    Firestore.collection("restaurant_users").doc(uid).get().then(doc => {
      if (doc.exists) {
        const restaurant_user = doc.data();
        const { restaurantId } = restaurant_user;
        Firestore.collection("restaurants").doc(restaurantId).get().then(restaurantDoc => {
          if (restaurantDoc.exists) {
            const restaurant = restaurantDoc.data();
            dispatch({type: TYPES.FETCH_RESTAURANT_SUCCESS, data: restaurant});
            return;
          }

          const message = 'Restaurant not found';
          toast.error(message);
        }).catch(err => {
          toast.error(err);
        });
        return;
      }
      
      const message = 'Restaurant user not found';
      toast.error(message);
    });
  }
}

let fetchOrdersSubscription = null;

export const fetchOrders = (restaurantId) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_ORDERS_REQUEST});
    fetchOrdersSubscription = Firestore.collection("orders").where("restaurantId", "==", restaurantId).onSnapshot(snapshot => {
      const data = snapshot.docs.map((val) => {
        return val.data();
      });
      dispatch({type: TYPES.FETCH_ORDERS_SUCCESS, data});
    });
  }
}

export const unsubscribeOrders = () => {
  fetchOrdersSubscription();
}

let fetchTablesSubscription = null;

export const fetchTables = (restaurantId) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_TABLES_REQUEST});
    fetchTablesSubscription = Firestore.collection("restaurant_table").where("restaurantId", "==", restaurantId).onSnapshot(snapshot => {
      const data = snapshot.docs.map((val) => {
        return val.data();
      });
      dispatch({type: TYPES.FETCH_TABLES_SUCCESS, data});
    });
  }
}

export const unsubscribeTables = () => {
  fetchTablesSubscription();
}

export const addUser = (data) => {
  return (dispatch) => {
    dispatch({type: TYPES.ADD_USER_REQUEST});
    data.id = uuidv4();
    Firestore.collection("users").doc(data.id).set(data)
    .then((snapshot) => {
      dispatch({type: TYPES.ADD_USER_SUCCESS});
      const { restaurantId, tableId, id } = data;
      const receiver = data.phoneNumber;
      const messageUrl = `https://smartordr.com/restaurant?id=${restaurantId}&tableId=${tableId}&userId=${id}`;
      const message = `Please click on following link ${messageUrl}`;
      const smsData = { receiver, message };
      SEND_SMS(smsData).then(res => {
        console.log(res);
      }).catch(err => {
        console.log(err);
      })
    }).catch((error) => {
      toast.error(error);
    });
  }
}

export const updateOrderStatus = (order, smsData) => {
  const { send, receiver, message } = smsData;
  return (dispatch) => {
    // dispatch({type: TYPES.UPDATE_ORDER_STATUS_REQUEST});
    Firestore.collection("orders").doc(order.id).update(order).then(res => {
      // dispatch({type: TYPES.UPDATE_ORDER_STATUS_SUCCESS});
      if (send) {
        const data = Object.assign({}, { receiver, message });
        SEND_SMS(data).then(res => {
          console.log(res);
        }).catch(err => {
          console.log(err);
        })
      }
    }).catch(err => {
      toast.error(err);
    })
  }
}

let fetchPaymentsSubscription = null;

export const fetchPayments = (restaurantId) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_PAYMENTS_REQUEST});
    fetchPaymentsSubscription = Firestore.collection("payments").where("restaurantId", "==", restaurantId).onSnapshot(snapshot => {
      const data = snapshot.docs.map((val) => {
        return val.data();
      });
      dispatch({type: TYPES.FETCH_PAYMENTS_SUCCESS, data});
    });
  }
}

export const unsubscribePayments = () => {
  fetchPaymentsSubscription();
}

let fetchReviewsSubscription = null;

export const fetchReviews = (restaurantId) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_REVIEWS_REQUEST});
    fetchReviewsSubscription = Firestore.collection("reviews").where("restaurantId", "==", restaurantId).onSnapshot(snapshot => {
      const data = snapshot.docs.map((val) => {
        return val.data();
      });
      dispatch({type: TYPES.FETCH_REVIEWS_SUCCESS, data});
    });
  }
}

export const unsubscribeReviews = () => {
  fetchReviewsSubscription();
}

export const getUserDetails = (order, onSuccess) => {
  const { customerId } = order;
  Firestore.collection("users").doc(customerId).get().then(doc => {
    if (doc.exists) {
      const user = doc.data();
      onSuccess(order, user);
      return;
    }

    toast.error('User not found');
  }).catch(err => {
    toast.error(err);
  })
}

export const addTable = (data) => {
  console.log(data)
  return (dispatch) => {
    // dispatch({type: TYPES.ADD_MENU_REQUEST});
    Firestore.collection("restaurant_table").doc(data.id).set(data)
    .then((snapshot) => {
      // dispatch({type: TYPES.ADD_MENU_SUCCESS});
    }).catch((error) => {
      toast.error(error);
    });
  }
}