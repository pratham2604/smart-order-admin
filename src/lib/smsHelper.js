import axios from 'axios';
import Config from '../web/config';
const CORS_URL = `https://cors-anywhere.herokuapp.com/`;
const SMS_URL = `https://api.msg91.com/api/v2/sendsms`

const SMS_DATA = {
  sender: "SMSIND",
  route: "4",
  country: "91",
  sms: []
}

const HEADERS = {
  "authkey": Config.SMS_AUTH_KEY,
  "content-type": "application/json",
};

export const SEND_SMS = (data) => {
  const { receiver, message } = data;
  const url = `${CORS_URL}${SMS_URL}`;
  const smsData = Object.assign({}, SMS_DATA);
  smsData.sms = [];
  smsData.sms.push({
    message,
    to: [receiver]
  });
  return new Promise((resolove, reject) => {
    axios.post(url, smsData, {headers: HEADERS}).then(res => {
      return resolove(true);
    }).catch(reject);
  })
}