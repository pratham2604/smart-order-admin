import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
import Config from '../web/config';

let firebaseInitialized = false;
firebase.initializeApp(Config.FIREBASE_CONFIG);
firebaseInitialized = true;

export const Firestore = firebaseInitialized ? firebase.firestore() : null;
export const Firebase = firebaseInitialized ? firebase : null;
export const FirebaseStorage = firebaseInitialized ? firebase.storage() : null;

export const LoadFirebase = () => {
  console.log('Firebase initialised.');
}

export const CreateFirebaseApp = () => {
  return firebase.initializeApp(Config.FIREBASE_CONFIG, 'Secondary');
}