import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react';
import _ from 'lodash';

class DataTable extends Component {
  state = {
    currentSortField: '',
    sortAscend: true,
  }

  handleSort = (field) => {
    const { currentSortField, sortAscend } = this.state;
    this.setState({
      currentSortField: field,
      sortAscend: field === currentSortField ? !sortAscend : true,
    })
  }

  render() {
    const { data } = this.props;
    const { columns, rows } = data;
    const { currentSortField, sortAscend } = this.state;
    const direction = sortAscend ? 'asc' : 'desc';
    const filteredRows = _.orderBy(rows, [currentSortField], [direction]);

    return (
      <div className="table-container">
        <table className="table">
          <thead className="table-header">
            <tr>
              {columns.map(column => {
                const { title, sort, field } = column;
                return (
                  <th className="header-cell" onClick={sort && this.handleSort.bind(this, field)} key={field}>
                    {title}
                    {sort && (
                      currentSortField === field ?
                      sortAscend ? <Icon name="sort ascending"/> : <Icon name="sort descending"/> :
                      <Icon disabled name="sort"/>
                    )}
                  </th>
                )
              })}
            </tr>
          </thead>
          {filteredRows.length ?
            <tbody>
              {filteredRows.map((dataRow, index) => {
                return (
                  <tr className="table-row" key={index}>
                    {columns.map(column => {
                      const { field } = column;
                      const value = dataRow[field];
                      return (
                        <td className="table-cell" key={field}>
                          {value}
                        </td>
                      )
                    })}
                  </tr>
                )
              })}
            </tbody> :
            <tbody>
              <tr>
                <td colSpan={columns.length}>
                  No items to show
                </td>
              </tr>
            </tbody>
          }
        </table>
      </div>
    )
  }
}

export default DataTable;