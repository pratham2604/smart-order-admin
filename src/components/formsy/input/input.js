import React, { Component } from 'react';
import { withFormsy } from 'formsy-react';
import { Form, Input } from 'semantic-ui-react';
import cx from 'classnames';
import { getFormError, resetInputValidation, validateForError } from '../helper/index';

class FormsyInput extends Component {
  state = {
    isValueInValid: false,
    error: '',
  };

  componentDidMount() {
    const { value, setValue } = this.props;
    value && setValue(value || '');
  }

  onChange = (event) => {
    const { name, value } = event.currentTarget;
    const { onChange } = this.props;

    this.setState({
      isValueInValid: false,
      error: '',
    });

    resetInputValidation(this.props);
    this.props.setValue(value);
    onChange && onChange(name, value);
  }

  onBlur = (event) => {
    const { name, value } = event.currentTarget;
    const { onBlur } = this.props;
    const error = validateForError(this.props, value);

    if (error) {
      this.setState({
        isValueInValid: true,
        error,
      });
    }

    onBlur && onBlur(name, value);
  }

  render() {
    const { name, label, type, placeholder, defaultValue, formValidations, className, value } = this.props;
    const { fluid, icon, iconPosition } = this.props;
    const { error, isValueInValid } = this.state;
    const formError = getFormError(this.props, formValidations);
    const isFormError = !!formError;
    const formClass = cx('form-input', className);

    return (
      <Form.Field>
        <Input
          className={formClass}
          error={isValueInValid || isFormError}
          name={name}
          type={type}
          label={label}
          placeholder={placeholder}
          onChange={this.onChange}
          onBlur={this.onBlur}
          value={value || defaultValue || ''}
          fluid={fluid}
          icon={icon}
          iconPosition={iconPosition}
        />
        {error && <div className="error">{error || formError}</div>}
      </Form.Field>
      
    );
  }
}

export default withFormsy(FormsyInput);
