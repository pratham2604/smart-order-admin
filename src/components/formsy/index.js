import Form from './form/form';
import Input from './input/input';
import RadioButtons from './radioButtons/index';
import ImageUpload from './imageUploader/imageUploader';
import Checkbox from './checkbox/checkbox';
import TextArea from './textArea/textArea';
import Search from './search/search';
import Select from './select/select';
import PhoneInput from './phoneInput/input';
import SubmitButton from './submitButton/submitButton';

export default {
  Form,
  Input,
  RadioButtons,
  ImageUpload,
  Checkbox,
  TextArea,
  Search,
  Select,
  PhoneInput,
  SubmitButton,
};
