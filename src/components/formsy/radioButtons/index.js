import React, { Component, Fragment } from 'react';
import { withFormsy } from 'formsy-react';
import { Form, Button, Grid, Header, Segment } from 'semantic-ui-react';

class RadioButtons extends Component {
  state = {};

  componentDidMount() {
    const { value, setValue, options } = this.props;
    const defaultValue = options[0].value;
    setValue(value || defaultValue);
  }

  onChange = (value) => {
    const { onChange, name } = this.props;

    this.setState({
      isValueInValid: false,
      error: '',
    });

    this.props.setValue(value);
    onChange && onChange(name, value);
  }

  render() {
    const { label, value, options, showBorder = true } = this.props;

    const radioButtonComponent = (
      <Form.Field>
        <Grid textAlign="left">
          {label &&
            <Grid.Column mobile={16} tablet={12} computer={4} verticalAlign="middle">
              <Header as="h5">{label}</Header>
            </Grid.Column>
          }
          <Grid.Column mobile={16} tablet={12} computer={12}>
            <Button.Group>
              {options.map((option, index) => {
                const lastIndex = options.length - 1;
                const { label } = option;
                const positive = option.value === value;
                return (
                  <Fragment key={index}>
                    <Button positive={positive} type="button" onClick={this.onChange.bind(this, option.value)}>{label}</Button>
                    {index < lastIndex && <Button.Or />}
                  </Fragment>
                )
              })}
            </Button.Group>
          </Grid.Column>
        </Grid>
      </Form.Field>
    );

    return (
      <Fragment>
        {showBorder ?
          <Segment>
            {radioButtonComponent}
          </Segment>:
          radioButtonComponent
        }
      </Fragment>
    );
  }
}

export default withFormsy(RadioButtons);
