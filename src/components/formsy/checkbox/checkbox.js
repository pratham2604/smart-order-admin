import React, { Component } from 'react';
import { withFormsy } from 'formsy-react';
import { Checkbox, Form, Grid, Header } from 'semantic-ui-react';

class FormsyCheckbox extends Component {
  state = {};

  componentDidMount() {
    const { value, setValue } = this.props;
    const defaultValue = value ? value : false;
    setValue(defaultValue);
  }

  onChange = (e) => {
    const { onChange, name, value, setValue } = this.props;
    setValue(!value);
    onChange && onChange(name, !value);
  }

  render() {
    const { label, value, name, toggle = false } = this.props;

    return (
      <Form.Field>
        <Grid textAlign="left">
          {label &&
            <Grid.Column mobile={16} tablet={8} computer={4} verticalAlign="middle">
              <Header as="h5">{label}</Header>
            </Grid.Column>
          }
          <Grid.Column mobile={16} tablet={8} computer={4}>
            <Checkbox toggle={toggle} name={name} checked={value} onChange={this.onChange} />
          </Grid.Column>
        </Grid>
      </Form.Field>
    );
  }
}

export default withFormsy(FormsyCheckbox);
