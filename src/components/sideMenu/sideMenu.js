import React, { Component, Fragment } from 'react';
import { Menu, Header } from 'semantic-ui-react';
import { Link, withRouter } from 'react-router-dom';
import cx from 'classnames';
import ImageHolder from '../UI/imageHolder/imageHolder';

const routes = [{
  title: 'Dashboard',
  to: '/',
  id: 'home',
  icon: 'icon-Dashboard'
}, {
  title: 'Menu',
  to: '/menu',
  id: 'menu',
  icon: 'icon-Menu',
}, {
  title: 'Table',
  to: '/table',
  id: 'tables',
  icon: 'icon-Table',
}];

const TYPE_MENU_MAP = {
  hotel: 'Menu',
  shop: 'Catalogue',
  services: 'Services',
  others: 'Catalogue'
};

const TYPE_TABLE_MAP = {
  hotel: 'Room',
  shop: 'Locations',
  services: 'Locations',
  others: 'Locations'
}

class SideMenu extends Component {
  componentDidMount() {
    const { location } = this.props;
    const { pathname } = location;
    const currentRoute = routes.find(route => route.to === pathname) || {};
    this.props.onMenuChange(currentRoute.title || '');
  }

  onMenuChange = (path) => {
    this.props.onMenuChange(path);
  }

  render() {
    const { location, profileData } = this.props;
    const { pathname } = location;
    const { name = 'Title', description = 'Description', thumb = '', type = '' } = profileData || {};
    const finalMenuTitle = TYPE_MENU_MAP[type] || 'Menu';
    const finalTableTitle = TYPE_TABLE_MAP[type] || 'Tables';

    return (
      <Fragment>
        <Menu.Item className="side-menu-header-buffer">
        </Menu.Item>
        <Menu.Item as='a' className="side-menu-info-container">
          <ImageHolder size='medium' rounded src={thumb} />
          <Header as='h3' className="title">{name}</Header>
          <Header as='h4' className="description">{description}</Header>
        </Menu.Item>
        {routes.filter(route => {
          // Route filter condition
          return true;
        }).map((route, index) => {
          const { to, title, icon, id } = route;
          const iconClassName = cx('menu-icon', icon);
          const newTitle = id === 'menu' ? finalMenuTitle : id === 'tables' ? finalTableTitle: title;
          return (
            <Menu.Item as={Link} to={to} key={index} active={pathname === to} onClick={this.onMenuChange.bind(this, title)}>
              <span className={iconClassName}></span>{newTitle}
            </Menu.Item>
          )
        })}
      </Fragment>
    )
  }
}

export default withRouter(SideMenu);